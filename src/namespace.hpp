#ifndef STREAM9_LINUX_XATTR_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LINUX_XATTR_TEST_SRC_NAMESPACE_HPP

namespace stream9::linux {}
namespace stream9::filesystem {}
namespace stream9::strings {}
namespace stream9::json {}
namespace std::filesystem {}

namespace testing {

namespace st9 { using namespace stream9; }
namespace lx { using namespace st9::linux; }
namespace fs { using namespace st9::filesystem; }
namespace fs { using namespace std::filesystem; }
namespace str { using namespace st9::strings; }
namespace json { using namespace st9::json; }

} // namespace testing

#endif // STREAM9_LINUX_XATTR_TEST_SRC_NAMESPACE_HPP
