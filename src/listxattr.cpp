#include <stream9/linux/xattr/listxattr.hpp>

#include "namespace.hpp"

#include <stream9/linux/xattr/setxattr.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem.hpp>
#include <stream9/json.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(listxattr_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto p = fs::current_path() / "xxx";
        fs::temporary_file f { p.c_str() };

        auto names = lx::listxattr(f.path());

        BOOST_CHECK(names.begin() == names.end());
    }

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        auto p = fs::current_path() / "xxx";
        fs::temporary_file f { p.c_str() };

        lx::setxattr(f.path(), "user.test.x", "x");
        lx::setxattr(f.path(), "user.test.y", "y");
        lx::setxattr(f.path(), "user.test.z", "z");

        auto names = lx::listxattr(f.path());

        json::array expected {
            "user.test.x",
            "user.test.y",
            "user.test.z",
        };

        BOOST_CHECK(json::value_from(names) == expected);
    }

    BOOST_AUTO_TEST_CASE(nothrow_1_)
    {
        auto p = fs::current_path() / "xxx";
        fs::temporary_file f { p.c_str() };

        lx::setxattr(f.path(), "user.test.x", "x");
        lx::setxattr(f.path(), "user.test.y", "y");
        lx::setxattr(f.path(), "user.test.z", "z");

        auto o_names = lx::nothrow::listxattr(f.path());

        BOOST_REQUIRE(o_names);

        json::array expected {
            "user.test.x",
            "user.test.y",
            "user.test.z",
        };

        BOOST_CHECK(json::value_from(*o_names) == expected);
    }

    BOOST_AUTO_TEST_CASE(nothrow_2_)
    {
        auto o_names = lx::nothrow::listxattr("xxx");

        BOOST_CHECK(o_names == lx::errc::enoent);
    }

BOOST_AUTO_TEST_SUITE_END() // listxattr_

} // namespace testing
