#include <stream9/linux/xattr/getxattr.hpp>
#include <stream9/linux/xattr/setxattr.hpp>

#include "namespace.hpp"

#include <filesystem>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>
#include <stream9/filesystem.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(lsetxattr_lgetxattr_)

    BOOST_AUTO_TEST_CASE(get_attr_into_new_string_)
    {
        auto p = fs::current_path() / "xxx";
        fs::temporary_file f { p.c_str() };

        lx::lsetxattr(f.path(), "user.test.foo", "bar");

        auto const o_v = lx::lgetxattr(f.path(), "user.test.foo");

        BOOST_REQUIRE(o_v);
        BOOST_TEST(o_v->size() == 3);
        BOOST_TEST(*o_v == "bar");
    }

    BOOST_AUTO_TEST_CASE(get_nonexistent_attr_into_new_string_)
    {
        auto p = fs::current_path() / "xxx";
        fs::temporary_file f { p.c_str() };

        auto const o_v = lx::lgetxattr(f.path(), "user.test.foo");

        BOOST_REQUIRE(!o_v);
    }

BOOST_AUTO_TEST_SUITE_END() // lsetxattr_lgetxattr_

} // namespace testing
