#include <stream9/linux/xattr/getxattr.hpp>
#include <stream9/linux/xattr/setxattr.hpp>

#include "namespace.hpp"

#include <filesystem>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>
#include <stream9/filesystem.hpp>

#include <sys/xattr.h>

namespace testing {

using stream9::print_error;

BOOST_AUTO_TEST_SUITE(setxattr_getxattr_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto p = fs::current_path() / "xxx";
        fs::temporary_file f { p.c_str() };

        lx::setxattr(f.path(), "user.test.foo", "bar");

        auto const o_v = lx::getxattr(f.path(), "user.test.foo");

        BOOST_REQUIRE(o_v);
        BOOST_TEST(o_v->size() == 3);
        BOOST_TEST(*o_v == "bar");
    }

    BOOST_AUTO_TEST_CASE(get_nonexistent_attr_)
    {
        auto p = fs::current_path() / "xxx";
        fs::temporary_file f { p.c_str() };

        auto const o_v = lx::getxattr(f.path(), "user.test.foo");

        BOOST_REQUIRE(!o_v);
    }

    BOOST_AUTO_TEST_CASE(get_attr_with_empty_key_)
    {
        auto p = fs::current_path() / "xxx";
        fs::temporary_file f { p.c_str() };

        auto o_v = lx::getxattr(f.path(), "");

        BOOST_CHECK(!o_v);
    }

BOOST_AUTO_TEST_SUITE_END() // setxattr_getxattr_

} // namespace testing
